---
permalink: /
title: "Página web de divulgação acadêmica"
excerpt: "About me"
author_profile: true
redirect_from: 
  - /about/
  - /about.html
---

## O que temos aqui?

Olá visitante, seja bem-vindo e bem-vinda!!!!

 Essa página web é para que eu possa lhe apresentar algo sobre mim, com foco na divulgação da ciência e avanço de tecnologias. Pretendo tratar de contextos e tecnologias diversas como no caso de manufatura de softwares e dispositivos. 

Em resumo: 
> Tenho muito interesse me Ciências, Softwares Livres e Cultura Popular. 

## Breve apresentação

Fiz graduação em Física na Universidade Federal de Mato Grosso do Sul entre 2000 e 2005. A partir do 2o ano do curso, iniciei trabalho de iniciação científica PIBIC/CNPq. Permaneci neste projeto até o término da graduação, bem como outros, que também considero muito importante, por suas obras realizadas, como o projeto de cultura Sarandi Pantaneiro, o qual frequentei como dançarino durante todo o período de graduação e mestrado. 

Desempenhei a função de Professor de Ensino Médio e Cursinho Pré-Vestibular para as disciplinas de Matemática, Química e Física. 

No início de 2006, ingressei no Programa de Pós-graduação em Química. No final de 2007, defendi minha Dissertação de Mestrado intitulada “Estudo fotoeletroquímico e termoanalítico da eletrodeposição de prata em meios poliméricos para construção de dispositivo eletrocrômico” sob a orientação do Prof. Dr. Sílvio Cesar de Oliveira. Logo após, iniciei as atividades de pesquisa relativas ao meu doutoramento no Laboratório de Filmes Finos do IF/USP, sob a orientação do Profa. Dra. Maria Cecília Salvadori. Minha tese de doutorado foi defendida em outubro de 2011 - intitulada “Super-hidrofobia obtida através Microestruturas Litografadas”.

Minha formação básica foi toda realizada em escola pública: cursei o ensino fundamental na E.E. Primavera (1988-1995) e o ensino médio na E.E. Maria Audenir de Carvalho (1995-1999), em Primavera, SP.

Mas, sempre digo que a mais importante escola que frequentei foi o SENAI de Presidente Prudente, SP, onde fiz cursos rápidos de Mecânica de automóveis e motorização. 


