---
layout: archive
title: "CV"
permalink: /cv/
author_profile: true
redirect_from:
  - /resume
---

{% include base_path %}

Formação acadêmica
======
* Graduado em Física, UFMS
* Mestre em Físico-Química, UFMS
* Doutorado em Ciências - Área: Eng. Elétrica, USP

<!---
Incluir USP e link da tese de doutorado. 
your comment goes here
and here
-->

Profissão
======
* Professor de Física
  * Universidade Federal da Grande Dourados - UFGD
  * Orientador no Mestrado Profissional em Ensino de Física

Destaques
======
* M. R. S. Oliveira. Produção de Janelas Inteligentes baseadas em Polímeros naturais. In: Henrique Ajuz Holzmann. (Org.). Coleção desafios das engenharias: Engenharia de materiais e metalúrgica 2. 1a edição: Atena Editora, volume 2, páginas 84-93, 2021.
* Minicurso: "Introdução ao Latex" para a Semana Acadêmica de Engenharia Mecânica - UFGD - 2020. 
* M. R. S. Oliveira. Oficina Sobre a Temática da Promoção da Igualdade Racial para Comissões de Heteroidentificação da UFGD. 2018.
* [Livro Prêmio Igualdade Racial - São Paulo - 2011](https://marciorsoliveira.gitlab.io//files/2011-livro-premio-igualdade-sp.pdf) Premiado na categoria: Artigo Científico. 

