---
title: "Superfície contendo microestruturas de redes quadradas"
excerpt: "A figura abaixo contem uma micrografia MEV de um rede de estruturas quadriculadas de resina epoxe SU-8 sobre láminas de vidro para compor dispositivos com diferentes aplicações. 1<br/><img src='/images/30x-d4.png'>"
collection: portfolio
---

This is an item in your portfolio. It can be have images or nice text. If you name the file .md, it will be parsed as markdown. If you name the file .html, it will be parsed as HTML. 
