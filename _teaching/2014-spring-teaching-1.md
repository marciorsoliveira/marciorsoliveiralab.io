---
title: "Experiência com Ensino Superior"
collection: teaching
type: "Ensino Superior"
permalink: /teaching/2014-spring-teaching-1
venue: "Universidade Federal da Grande Dourados, Área de Física"
date: 2021-01-01
location: "Dourados, MS - Brasil"
---
## Principais disciplinas
* Laboratório de Física 1, 3 e 4
* Resistência dos Materiais
* Ciência dos Materiais
* Física 1, 2, 3 e 4 
* Quântica -- Introdução
