---
title: 'UFGD, uma Universidade de ponta, muito alem das fronteiras'
date: 2021-12-17
permalink: /posts/2012/08/blog-post-1/
tags:
  - UFGD
  - Dourados
  - Cultura popular
---

> "Quero viver  
> Muito além das fronteiras  
> Dos que só sabem ser  
> Pedras de atiradeira  
> Eu devia saber  
> Que de certa maneira  
> Não seremos jamais  
> Mais que grãos de poeira  
> No céu   

(Almir Sater & Renato Teixeira)

Para falar da Universidade Federal da Grande Dourados, UFGD, precisamos nos localizar em um mapa. O Mato Grosso do Sul, um estado tão grande quanto todo o território da Alemanha, é marcados por suas diversidade cultural que se expressa magestosamente nas artes e na culpura popular. As violadas, catiras, rasqueados são únicos e especiais. Musica de origens tão variadas quantos suas fronteiras: 5 estados e 2 países.

Neste clipe de um Festival de Cultura Popular da UFGD, nos já longincos 2019, o artista Almir Sater, executa uma música que citei os versos aqui no começo, aos arranjos de violas e samfonas que apesar de moderna reporta ao espirito, mistico, religioso e rural das cavalgadas e vaquejadas da região. Neste evento realizado no pátio da UFGD não havia nenhuma fogueira, mas nesta noite fria, todos podiam sentir o aconchego do acalanto brasil dessa música encandecente. 

Prezados, leitores, alunos e amigos, isto é um pouco do ambiente universitário da UFGD antes da pandemia e dos golpes que as universidades sofreram. Vale à pena destacar que a região, como em quase todo o pais, é carente de eventos culturais e a Universidade é reconhecida como uma das maiores produtoras de eventos culturais de baixo custo ou grátis. 

Copio aqui o clip de divulgação do Festival por ser esse artista reconhecido como um simbolo da musica do MS: Almir Sater. 

Observem esse cantor -- Alir Sater. O geito calmo e elegante de conduzir o Show... isso reporta ao comportamento geral da população que é de modo geral calma e discreta, como se diz por aqui: como bons Guaranis. A acendencia japones e asiatica bem como árabe na cidade também é notada nesse gestos serenos e tranquilos. Uma exeção que se faz notar é a presena sulista, em especial, aos sulistas de origem italiana, que são mais eloquentes e falantes. Na rua ou no comercio se ve rapido quem é quem. Mas de modo geral podemos dizer que a cidade de Dourados tem essa atimosfera serena e meditativa que se observa nesse show. 

Reparem, no artista, o modelo do Chapeu ao bom estilo Santos Dumont, daquele chapel castigado por Sol e chuva. O artista não usa os sintos com fivelas de grande comércio, mas sim um típica faixa Pantaneira. A vantagem é que alem de muito elegante, evita que as fivelas metálicas dos cintos danifique o instrumento que aqui é sagrado. A viola é sagrada. Reparem pois que o violões e violas dos artistas em outros contextos tem pequenos dandos ou riscos, na parte de traz do instrumento, devio ao contato com o metal das fivels dos sintos. O arremate da faicha na sintura também se distingue do estilo argentino ou gaucho.  

Olha eu ai. Nesse clip eu sou o pai que aparece na verso "Pai com facão Guarani". Como poder se ver no vídeo, nesse momento eu me esfoçava para ensinar o famoso passo de Guarania (Chamamé) para a minha filha mais velha enquanto que a filha mais nova, de certo por siumes, buscou avacalhar essa momento de instrução, para a elforia e aplausos dos observadores de suas micagens.  

Para que voces tenham um idéa como esses objetos do contidiano, como um fação, movimentam a memória afetiva de um povo inteiro,  vou encerrar esses comentários sobre a UFGD aqui citando uma outra música sobre a relação entre o caipira e seu fação dos compositores: Murilo Antunes & Tavinho Moura, eternizada na voz de Teodoro & Sampaio. Note da presença cultural do facão, com instrumento de trabaho, exibicionismo e até masmo de autodefesa. 

Hoje a UFGD é o nosso fação Guarani. Não deixemos que ela se quebre ao meio. Que possamos preserva-la e com ela trilhar um futuro. 

> Meu facão guarani quebrou na ponta
> Quebrou no meio
> Eu falei pra morena que o
> Trem tá feio, ia, ie, ia...Oia

> Palha, forro e fumo de rolo
> Tudo é motivo pra meu facão
> Arma de pobre é fome, é facão

> Abre semente
> Aperta inimigo
> Espeta até gavião

> Corta sabugo e lança um desafio
> Não conta nem até três
> Que o trem tá feio é bem por aqui"

(Murilo Antunes & Tavinho Moura)

https://www.letras.mus.br/almir-sater-e-renato-teixeira-ar/d-de-destino/

https://www.youtube.com/watch?v=IBFURyeuOfw

------
