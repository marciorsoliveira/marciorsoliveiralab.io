---
title: "Fluorescence analysis of iodinated acetophenone derivatives"
collection: publications
permalink: /publication/2015-10-01-paper-title-number-3
date: 2015-10-01
venue: 'Spectrochimica Acta - Part A: Molecular and Biomolecular Spectroscopy'
citation: ' Fluorescence analysis of iodinated acetophenone derivatives
Crivelaro F., Oliveira M.R.S., Lima S.M., Andrade L.H.C., Casagrande G.A., Raminelli C., Caires A.R.L.
(2015)  Spectrochimica Acta - Part A: Molecular and Biomolecular Spectroscopy,  139  (1) , pp. 63-67.'
---
