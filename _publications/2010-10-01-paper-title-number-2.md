---
title: "Synthesis and characterization of TiO2 and TiO2/Ag for use in photodegradation of methylviologen, with kinetic study by laser flash photolysis"
collection: publications
permalink: /publication/2010-10-01-paper-title-number-2
date: 2015-01-01
venue: 'Environmental Science and Pollution Research volume'
citation: 'Crivelaro F., Oliveira M.R.S., Lima S.M., Andrade L.H.C., Casagrande G.A., Raminelli C., Caires A.R.L. (2015), Spectrochimica Acta - Part A: Molecular and Biomolecular Spectroscopy,  139  (1), pp. 63-67.'
---

This paper reports the synthesis, characterization, and application of TiO2 and TiO2/Ag nanoparticles for use in photocatalysis, employing the herbicide methylviologen (MV) as a substrate for photocatalytic activity testing. At suitable metal to oxide ratios, increases in silver surface coating on TiO2 enhanced the efficiency of heterogeneous photocatalysis by increasing the electron transfer constant. The sol–gel method was used for TiO2 synthesis. P25 TiO2 was the control material. Both oxides were subjected to the same silver incorporation process. The materials were characterized by conventional spectroscopy, SEM micrography, X-ray diffraction, calculation of surface area per mass of catalyst, and thermogravimetry. Also, electron transfers between TiO2 or TiO2/Ag and MV in the absence and presence of sodium formate were investigated using laser flash photolysis. Oxides synthesized with 2.0 % silver exhibited superior photocatalytic activity for MV degradation.

[Download paper here](https://link.springer.com/article/10.1007%2Fs11356-014-2678-1)

Cite this article:

Ramos, D.D., Bezerra, P.C.S., Quina, F.H. et al. Synthesis and characterization of TiO2 and TiO2/Ag for use in photodegradation of methylviologen, with kinetic study by laser flash photolysis. Environ Sci Pollut Res 22, 774–783 (2015). https://doi.org/10.1007/s11356-014-2678-1
