---
title: "Físicos Negros: Promovendo a Diversidade por meio de Associações"
collection: publications
permalink: /publication/2009-10-01-paper-title-number-1
excerpt: 'Esse artigo discute a densidade de Físico Negros na Física e nas Carreiras Física bem como aponta para as possibilidades de expansão a presença da Física na sociedade por meio de ampliação da igualdade de oportunidades e outras ações políticas.'
date: 2017-06-01
venue: 'Revista da ABPN'
paperurl: 'http://marciorsoliveira.gitlab.io/files/artigo-fisica-abpn.pdf'
---
A Comunidade de Física Brasileira exerce papel central nos programas prioritários da Estratégia Nacional de Ciência e Tecnologia, mas sofre com as limitações impostas pelas desigualdades raciais que marcam a sociedade brasileira. Neste trabalho discuto a atuação das associações de físicos negros no Brasil e no mundo como uma estratégia eficaz para o enfrentamento das desigualdades raciais e racismo nas áreas das Ciências. É discutido o mapeamento dos físicos negros no Brasil e a sub-repesentação destes negros na esfera acadêmica e profissional, em que pelo menos metade dos físicos não estão congregados em nenhuma das associações tradicionais das carreiras em Física. Os esforços de associações e encontros brasileiros de físicos e cientistas negros foram identificados pelo interesse comum na democratização e aumento da diversidade do ensino superior e oportunidades oferecidas pela carreira de Físico. Em colaboração com os movimentos sociais negros, esses encontros e associações, buscam revelar novos talentos, motivar estudantes negros a ingressarem na carreira de física e combater estereótipos da profissão. Essas organizações de físicos e cientistas negros demonstram grande expectativa em acelerar ou antecipar resultados de programas nacionais de inclusão de negros com as Ações Afirmativas, promover a circulação, visibilidade e empoderamento de negros na Física, superando com isso a subalternidade e invisibilidade dos negros nas carreiras físicas.

[Artigo completo aqui.](http://marciorsoliveira.gitlab.io/files/artigo-fisica-abpn.pdf)

Como Citar: 
* OLIVEIRA, Marcio Roberto da Silva. FÍSICOS NEGROS: PROMOVENDO A DIVERSIDADE POR MEIO DE ASSOCIAÇÕES. Revista da Associação Brasileira de Pesquisadores/as Negros/as (ABPN), [S.l.], v. 9, n. 22, p. 206-227, jun. 2017. ISSN 2177-2770. Disponível em: <https://abpnrevista.org.br/index.php/site/article/view/405>. Acesso em: dd mm aaaa. 
